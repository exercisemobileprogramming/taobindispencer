import 'dart:io';
import 'Drink.dart';

class Stock{
  var name;
  List<Drink> stock=[];
  late num price;
  Stock(){
    
  }

  addAllDrink(List<Drink> product ){  
    stock.addAll(product);
  }

  addDrink(Drink product){
    stock.add(product);
  }

  DeleteDrink(int indexOfDrink){
    stock.removeAt(indexOfDrink);  
  }


  void showStock(){
    for(int i=0;i<stock.length;i++){
        print(stock.elementAt(i).toString()+" : "+"กด ${i+1}"+ " ");
    }
  }

  bool process(){
    showStock();
    showChooseNumber();
    int x = int.parse(stdin.readLineSync()!)-1;
    var indexStock = stock.elementAt(x);
    late String tempTypeOfDrink = "";
      for(int i =0;i<stock.length;i++){
      if(x==i){
        print("คุณเลือก ${indexStock.getName()}");
        tempTypeOfDrink = chooseTypeOfDrink(indexStock, tempTypeOfDrink);//เลือกประเภทของเครื่องดื่ม
        setPrice(indexStock);   //set ราคาตามประเภทเครื่องดื่ม
        var Sweetness = chooseSweetness(indexStock);
        getProduct(indexStock, tempTypeOfDrink, Sweetness);  
        checkBill();         //คำนวณเงินที่ได้รับและเงินทอน
        showThxAndWait();
        showExitOrContin();
        int z=int.parse(stdin.readLineSync()!);
        if(z==1){
          return true; 
        }else if(z==0){
          return false;
        }
      }
    }
    return false;
  }

  void getProduct(Drink indexStock, String tempTypeOfDrink, String? Sweetness) {
    print("คุณได้เลือก ${indexStock.getName()} แบบ : $tempTypeOfDrink ระดับความหวาน : $Sweetness  ราคา : $price ");
  }

  void showExitOrContin() {
    print("ต้องการเครื่องดื่มเพิ่ม กด : 1 ออกจากหน้าจอ กด : 0");
  }

  void showThxAndWait() {
    print("ขอบคุณที่ใช้บริการเต่าบิน กรุณารอสักครู่ เพื่อรอเครื่องดื่ม");
  }

  //คำนวณเงิน
  void checkBill() {  
    stdout.write("กรุณาใส่เงินสด : "); 
    num cash = num.parse(stdin.readLineSync()!);
    print("รับเงินมา: $cash");
    print("รับเงินทอน : ${cash-price}");
  }

    //set ราคาตามประเภทเครื่องดื่ม
  void setPrice(Drink indexStock) {
    int indexOfPrice = int.parse(stdin.readLineSync()!);
    switch(indexOfPrice){
      case 1:
      price = indexStock.getH_Price();
      break;
      case 2:
      price = indexStock.getF_Price();
      break;
      case 3 :
      price = indexStock.getS_Price();
      break;
      default:
    } 
  }
  //เลือกประเภทของเครื่องดื่ม
  String chooseTypeOfDrink(Drink indexStock, String tempTypeOfDrink) {
    print("โปรดเลือกประเภทเครื่องดื่ม");
    if(indexStock.getH_Price()!=0){
      print("ร้อน: กด 1");
      tempTypeOfDrink="ร้อน";
    }
    if(indexStock.getF_Price()!=0){
      print("เย็น: กด 2");
      tempTypeOfDrink="เย็น";
    }
    if(indexStock.getS_Price()!=0){
      print("ปั่น: กด 3");
      tempTypeOfDrink="ปั่น";
    }
    return tempTypeOfDrink;
  }
  //โชว์ ข้อความและ เลือกความหวาน
  String? chooseSweetness(Drink indexStock) {
    String? x ;
    print("โปรดเลือกระดับความหวาน");
    if(indexStock.getSweetness1()){
      print("ไม่ใส่น้ำตาล : กด 1 ");
    }else{
      print("ไม่ใส่น้ำตาล : ไม่มี");
    }
    if(indexStock.getSweetness2()){
      print("หวานน้อย : กด 2 ");
    }else{
      print("หวานน้อย : ไม่มี");
    }
    if(indexStock.getSweetness3()){
      print("หวานปกติ : กด 3 ");
    }else{
      print("หวานปกติ : ไม่มี");
    }
    if(indexStock.getSweetness4()){
      print("หวานมาก : กด 4 ");
    }else{
      print("หวานมาก : ไม่มี");
    }
    if(indexStock.getSweetness5()){
      print("หวาน 3 โลก : กด 5 ");
    }else{
      print("หวาน 3 โลก : ไม่มี");
    }
    var choose = int.parse(stdin.readLineSync()!);
    switch(choose){
          case 1:
          if(indexStock.getSweetness1()){
            x="ไม่ใส่น้ำตาล";
          }else{
            x= "ไม่มี";
          }
          break;
          case 2:
          if(indexStock.getSweetness2()){
            x="หวานน้อย";
          }else{
            x= "ไม่มี";
          }
          break;
          case 3 :
          if(indexStock.getSweetness3()){
            x="หวานปกติ";
          }else{
            x= "ไม่มี";
          }
          break;
          case 4 :
          if(indexStock.getSweetness4()){
            x="หวานมาก";
          }else{
            x= "ไม่มี";
          }
          break;
          case 5 :
          if(indexStock.getSweetness5()){
            x="หวาน 3 โลก";
          }else{
            x= "ไม่มี";
          }
          break;
          default:
        } 
    return x;
  }

  void showChooseNumber(){
    stdout.write("\n กรุณาเลือกเครื่องดื่มที่ต้องการ :");
  }
  String getNameOfStock() => name;

  @override
  String toString() {
    // TODO: implement toString
    return stock.toString();
  }
}