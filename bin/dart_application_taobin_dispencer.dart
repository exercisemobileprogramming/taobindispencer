import 'dart:io';
import 'Drink.dart';
import 'Stock.dart';
import 'Dispencer.dart';


void main() {
  var coffeeList = <Drink> [Drink("กาแฟดำ",true,true,true,true,false,30,35,0),Drink("เอสเพรสโซ่",true,true,true,true,false,30,0,0),
                        Drink("เอสเย็นแบบไทยๆ",true,true,true,true,true,0,45,50),Drink("คาปูชิโน่",true,true,true,true,true,35,40,45)];
  var teaList = <Drink> [Drink("เก๊กฮวย",true,true,true,true,true,20,25,30),Drink("ชาขิงมะนาว",true,true,true,false,false,20,0,0),
                        Drink("ชาดำ",false,true,true,true,false,25,30,0),Drink("ชาเขียวญี่ปุ่น",true,true,true,true,true,25,35,40)];
  Stock stock = new Stock();
  // Stock Tea = new Stock("Tea");
  stock.addAllDrink(coffeeList);
  stock.addAllDrink(teaList);
  Dispencer Taobin=Dispencer("Taobin");
  
  // print(coffeeStock.toString());
  // print(Taobin.getNameOfDis());
  // Taobin.addStock(Coffee);
  // Taobin.addStock(Tea);
  // print(Taobin.toString());

  Taobin.showWelcome();
  while(stock.process());
  

  // while(Taobin.process()){ // ใช้ While เพื่อตรวจสอบว่าเป็นตัวเลขที่ กำหนดไว้หรือไม่
  // }
}

