
class Drink{
  late String name;
  int cup = 1;
  bool sweetNess_1 = false;  //ไม่ใส่น้ำตาล
  bool sweetNess_2 = false;  //หวานน้อย
  bool sweetNess_3 = false;  //หวานปกติ
  bool sweetNess_4 = false;  //หวานมาก
  bool sweetNess_5 = false;  //หวาน 3 โลก
  num Htype=0; //H=ร้อน  
  num Ftype=0; //F=เย็น
  num Stype=0; //S=ปั่น

  Drink(String name, bool sweetNess,bool sweetNess_2,bool sweetNess_3,bool sweetNess_4,bool sweetNess_5,
        num Htype,num Ftype,num Stype){

    this.name=name;
    this.sweetNess_1=sweetNess; 
    this.sweetNess_2=sweetNess_2; 
    this.sweetNess_3=sweetNess_3; 
    this.sweetNess_4=sweetNess_4; 
    this.sweetNess_5=sweetNess_5;
    this.Htype=Htype;
    this.Ftype=Ftype;
    this.Stype=Stype; 

  }

  num getH_Price() => Htype; //ขอราคาของเครื่องดื่มที่เป็นแบบ ชนิดร้อน
  num getF_Price() => Ftype; //ขอราคาของเครื่องดื่มที่เป็นแบบ ชนิดเย็น
  num getS_Price() => Stype; //ขอราคาของเครื่องดื่มที่เป็นแบบ ชนิดปั่น

  bool getSweetness1() => sweetNess_1;
  bool getSweetness2() => sweetNess_2;
  bool getSweetness3() => sweetNess_3;
  bool getSweetness4() => sweetNess_4;
  bool getSweetness5() => sweetNess_5;

  String getSSweet1(){  //แสดงข้อมูลว่ามีหรือไม่มี ในระดับความหวาน ที่ไม่ใส่น้ำตาล
    if(sweetNess_1){
      return "มี";
    }else{
      return "ไม่มี";
    }
  }

  String getSSweet2(){   //แสดงข้อมูลว่ามีหรือไม่มี ในระดับความหวาน ที่หวานน้อย
    if(sweetNess_2){
      return "มี";
    }else{
      return "ไม่มี";
    }
  }

  String getSSweet3(){   //แสดงข้อมูลว่ามีหรือไม่มี ในระดับความหวาน ที่ หวานปกติ
    if(sweetNess_3){
      return "มี";
    }else{
      return "ไม่มี";
    }
  }

  String getSSweet4(){    //แสดงข้อมูลว่ามีหรือไม่มี ในระดับความหวาน ที่ หวานมาก
    if(sweetNess_4){
      return "มี";
    }else{
      return "ไม่มี";
    }
  }

  String getSSweet5(){     //แสดงข้อมูลว่ามีหรือไม่มี ในระดับความหวาน ที่ หวาน 3 โลก
    if(sweetNess_5){
      return "มี";
    }else{
      return "ไม่มี";
    }
  }

  int getCup() => cup;

  int addCup() => cup++;

  String  getName() => name;

  @override
  String toString() {
    // TODO: implement toString
    return "เครื่องดื่ม:"+getName()+" " + "\n ร้อน : "+ getH_Price().toString() + " "+ "เย็น : " 
           +getF_Price().toString()+ " "+ "ปั่น : " +getS_Price().toString()+"\n";
  }
}